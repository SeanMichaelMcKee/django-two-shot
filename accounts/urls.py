from django.urls import path
from accounts.views import user_login, user_logout, user_signup
from django.shortcuts import redirect


def redirect_to_home(request):
    return redirect("home")


urlpatterns = [
    path("signup/", user_signup, name="signup"),
    path("login/", user_login, name="login"),
    path("", redirect_to_home),
    path("logout/", user_logout, name="logout"),
]
